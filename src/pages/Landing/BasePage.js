import React from 'react';
import PropType from 'prop-types';

const BasePage = ({ contentProp, content, ...others }) => (
  <main {...others}>
    {content(contentProp)}
  </main>
);

BasePage.propTypes = {
  contentProp: PropType.shape({}),
  content: PropType.func,
};

BasePage.defaultProps = {
  content: () => null,
  contentProp: {},
};

export default BasePage;
