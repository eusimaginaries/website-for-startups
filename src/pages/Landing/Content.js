import React from 'react';
import PropType from 'prop-types';
import { Headline, Grid, BackgroundImage } from '../../components';

const gridHeadline = {
  key: 'grid-headline',
  component: ({ headlineProp }) => (
    <Headline {...headlineProp} />
  ),
};

const gridHeadline2 = {
  key: 'grid-headline-2',
  component: ({ headlineProp }) => (
    <Headline {...headlineProp} />
  ),
};


const Content = props => (
  <BackgroundImage
    {...props}
    component={({ gridProp, ...others }) => (
      <div {...others}>
        <Grid items={[gridHeadline, gridHeadline2]} {...gridProp} />
      </div>
    )}
  />
);

Content.propTypes = {
  gridProp: PropType.shape({}),
};

Content.defaultProps = {
  gridProp: {},
};

export default Content;
