import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';

import Page, { BasePage } from './index';

const requiredProps = { backgroundImageProp: { imgSrc: '' } };

describe('Crash tests', () => {
  it('renders without crashing (vanilla)', () => {
    const div = document.createElement('div');
    ReactDOM.render(<BasePage />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders without crashing (w/ props)', () => {
    const props = {
      sampleprop: 'true',
    };

    const div = document.createElement('div');
    ReactDOM.render(<BasePage {...props} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders without crashing (w/ content, w/o props)', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Page contentProp={{ ...requiredProps }} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders without crashing (w/ content, w/ props)', () => {
    const props = {
      ...requiredProps,
      sampleprop: 'true',
    };

    const div = document.createElement('div');
    ReactDOM.render(<Page contentProp={{ ...props }} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
});

describe('Functionality tests', () => {
  it('Returns w/o content', () => {
    const content = undefined;
    const expectedOutput = (
      <main />
    );
    expect(renderer.create(<BasePage content={content} />).toJSON())
      .toEqual(renderer.create(expectedOutput).toJSON());
  });

  it('Returns w/ content', () => {
    const content = () => <p />;
    const expectedOutput = (
      <main>
        <p />
      </main>
    );
    expect(renderer.create(<BasePage content={content} />).toJSON())
      .toEqual(renderer.create(expectedOutput).toJSON());
  });

  it('Returns w/ content w/ main props', () => {
    const content = () => <p />;
    const props = {
      className: 'main-div',
      contentProp: {},
    };
    const expectedOutput = (
      <main className="main-div">
        <p />
      </main>
    );
    expect(renderer.create(<BasePage content={content} {...props} />).toJSON())
      .toEqual(renderer.create(expectedOutput).toJSON());
  });

  it('Returns w/ content w/ content props', () => {
    const content = ({ p1Props, p2Props, ...others }) => (
      <div {...others}>
        <p {...p1Props} />
        <p {...p2Props} />
      </div>
    );
    const props = {
      contentProp: {
        className: 'content-main',
        p1Props: {
          className: 'content-p1',
        },
        p2Props: {
          className: 'content-p2',
        },
      },
    };
    const expectedOutput = (
      <main>
        <div className="content-main">
          <p className="content-p1" />
          <p className="content-p2" />
        </div>
      </main>
    );
    expect(renderer.create(<BasePage content={content} {...props} />).toJSON())
      .toEqual(renderer.create(expectedOutput).toJSON());
  });
});
