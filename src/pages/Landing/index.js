import React from 'react';
import BasePage from './BasePage';
import Content from './Content';

export { BasePage, Content };
export default props => (
  <BasePage
    content={contentProps => <Content {...contentProps} />}
    {...props}
  />
);
