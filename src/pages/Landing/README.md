# Landing Page #
## Description ##
The landing page is the very first page the user will see, therefore it have to be simple and impactful, showing only stuff that will entice the user to see more.
## Usage ##
```js
<Landing contentProp={props} />
```
## Props ##
- contentProp: **Object** Props configuration for the landing page content.
- content: **Function** Function callback to render the child components. `eg. (props) => <div {...props} />`.
## Composition ##
- Headline
- BackgroundImage