import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import { Typography } from '@material-ui/core';

import Component from './index';

describe('Crash tests', () => {
  it('renders without crashing (vanilla)', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Component />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders without crashing (w/ props)', () => {
    const props = {
      className: 'mainclass',
      titleProp: {
        className: 'titleComponent',
        text: 'This is the title component',
      },
      subtitleProp: {
        className: 'subtitleComponent',
        text: 'This is the subtitle component',
      },
    };

    const div = document.createElement('div');
    ReactDOM.render(<Component {...props} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders without crashing (w/ children)', () => {
    const div = document.createElement('div');
    ReactDOM.render((
      <Component>
        <div />
      </Component>
    ), div);
    ReactDOM.unmountComponentAtNode(div);
  });
});

describe('Functionality tests', () => {
  it('Returns with the correct main props', () => {
    const props = {
      className: 'main-div',
      randomProp: 'random random',
      titleProp: {},
    };
    const expectedOutput = (
      <div className="main-div" randomProp="random random">
        <Typography />
        <Typography />
      </div>
    );
    expect(renderer.create(<Component {...props} />).toJSON())
      .toEqual(renderer.create(expectedOutput).toJSON());
  });

  it('Returns with the correct title props', () => {
    const props = {
      titleProp: { className: 'title-typography', align: 'center' },
    };
    const expectedOutput = (
      <div>
        <Typography className="title-typography" align="center" />
        <Typography />
      </div>
    );
    expect(renderer.create(<Component {...props} />).toJSON())
      .toEqual(renderer.create(expectedOutput).toJSON());
  });

  it('Returns with the correct subtitle props', () => {
    const props = {
      subtitleProp: { className: 'subtitle-typography', align: 'center' },
    };
    const expectedOutput = (
      <div>
        <Typography />
        <Typography className="subtitle-typography" align="center" />
      </div>
    );
    expect(renderer.create(<Component {...props} />).toJSON())
      .toEqual(renderer.create(expectedOutput).toJSON());
  });

  it('Returns with the correct children rendered below subtitle', () => {
    const children = <p />;
    const expectedOutput = (
      <div>
        <Typography />
        <Typography />
        <p />
      </div>
    );
    expect(renderer.create(<Component>{children}</Component>).toJSON())
      .toEqual(renderer.create(expectedOutput).toJSON());
  });
});
