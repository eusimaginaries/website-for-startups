import React from 'react';
import PropType from 'prop-types';
import { Typography } from '@material-ui/core';


const Component = ({ titleProp, subtitleProp, children, ...others }) => (
  <div {...others}>
    <Typography {...titleProp}>{titleProp.text}</Typography>
    <Typography {...subtitleProp}>{subtitleProp.text}</Typography>
    {children}
  </div>
);

Component.propType = {
  titleProp: PropType.shape({
    text: PropType.string.isRequired,
  }).isRequired,
};

Component.defaultProps = {
  titleProp: {},
  subtitleProp: {},
};

export default Component;
