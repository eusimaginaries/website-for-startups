# Headline #
## Description ##
This component consist of 2 text fields.
- Title
- Subtitle
## Usage ##
```js
<Headline titleProp={titlePropObj} subtitleProp={subtitlePropObj >
  <div />
</Headline>
```
## Props ##
- titleProp: **Object**(Required) Props for the title typography component.
- titleProp.text: **String**(Required) Text for the title.
- subtitleProp: **Object** Props for the subtitle typography component.
- Sample Props:
```js
{
  className: 'mainclass',
  titleProp: {
    className: 'titleComponent',
    text: 'This is the title component',
  },
  subtitleProp: {
    className: 'subtitleComponent',
    text: 'This is the subtitle component',
  },
}
```
## Children ##
Children components are accepted as is and placed below the title and subtitle text.
