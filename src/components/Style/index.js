import BaseComponent from './BaseComponent';
import formatPropsClassname from './formatPropsClassname';
import Component from './Component';

export { BaseComponent, formatPropsClassname };
export default Component;
