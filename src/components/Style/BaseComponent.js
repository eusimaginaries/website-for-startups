import PropTypes from 'prop-types';

const BaseComponent = ({
  configProps,
  component,
  classes,
  formatter,
  ...others
}) => {
  const updatedConfigProps = configProps({ classes });
  const props = { ...others, ...updatedConfigProps };

  return component(formatter(props));
};

BaseComponent.propTypes = {
  configProps: PropTypes.func,
  classes: PropTypes.shape({}).isRequired,
  component: PropTypes.func.isRequired,
  formatter: PropTypes.func,
};

BaseComponent.defaultProps = {
  configProps: ({ classes }) => ({ classes }),
  formatter: props => props,
};

export default BaseComponent;
