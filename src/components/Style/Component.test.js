import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';

import Component, { BaseComponent, formatPropsClassname } from './index';

describe('Crash tests', () => {
  it('renders without crashing (vanilla)', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Component component={props => <div {...props} />} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders without crashing (w/ props)', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Component styles={{}} component={props => <div {...props} />} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
});

describe('Functionality tests', () => {
  it('Returns with the classNames', () => {
    const testClasses = { sample: 'Component-sample-1' };
    const testComponent = (
      <BaseComponent
        classes={testClasses}
        component={props => <div {...props} />}
        configProps={({ classes }) => ({
          className: `${classes.sample}`,
        })}
      />
    );
    expect(renderer.create(testComponent).toJSON())
      .toEqual(renderer.create(<div className="Component-sample-1" />).toJSON());
  });

  it('Returns with the inserts correct props, ignoring invalid classNames', () => {
    const testClasses = { sample: 'Component-sample-1' };
    const testComponent = (
      <BaseComponent
        classes={testClasses}
        component={props => <div {...props} />}
        configProps={({ classes }) => ({
          className: `class1 ${classes.sample} ${classes.donotexist}`,
          sample: 'true',
        })}
        formatter={formatPropsClassname}
      />
    );
    expect(renderer.create(testComponent).toJSON())
      .toEqual(renderer.create(<div className="class1 Component-sample-1" sample="true" />).toJSON());
  });
});

describe('formatPropsClassname tests', () => {
  it('Returns the original props', () => {
    const props = {
      className: 'classname1',
      obj1: {
        className: 'classname2',
        sample: 'true',
      },
    };

    expect(formatPropsClassname(props))
      .toEqual(props);
  });

  it('Returns the props removing invalid', () => {
    const props = {
      className: 'classname1 undefined',
      obj1: {
        className: 'classname2 undefined',
        sample: 'true',
      },
    };

    expect(formatPropsClassname(props))
      .toEqual({
        className: 'classname1',
        obj1: {
          className: 'classname2',
          sample: 'true',
        },
      });
  });
});
