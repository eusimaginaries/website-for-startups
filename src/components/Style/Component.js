import React from 'react';
import { withStyles } from '@material-ui/core';

import formatter from './formatPropsClassname';
import BaseComponent from './BaseComponent';
import styles from '../../config/styles';

const Component = props => (
  <BaseComponent
    {...props}
    formatter={formatter}
  />
);

export default withStyles(styles)(Component);
