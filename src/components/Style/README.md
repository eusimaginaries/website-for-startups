# Style #
## Description ##
This component is used to inject styling classes into the configuration class.
## Dependencies ##
`@core/material-ui/withStyle`
## Usage ##
```js
<Style styles={{}} component={props => <div {...props} />} />
```
## Props ##
- styles: **Object**(Required) JSON styles configurations that will be used as input for `withStyle`.
- component: **Function**(Required) component to be returned with the updated props. Must be in the following callback format: `props => <component {...props}/>`.
- configProps: **Function** JSON props configurations that `classes` from `withStyle` will be injected into. Therefore function should be similar to this format `({classes})=>{// ... main function ...}`.
- formatter: **Function** Pre formatting of props before returning in the component.
- Sample Props:
```js
{
  styles: {
    style1:{
      fontSize: 10,
    }
  },
  component: (props) => <div {...props} />,
  configProps: ({classes}) => ({classname: classes.style1}),
  formatter: (props) => (props),
}
```