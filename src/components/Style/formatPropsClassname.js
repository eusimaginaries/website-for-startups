/**
 * This module is used to ensure that the classnames are correct
 */
import classnames from 'classnames';

const formatPropsClassnameBase = (formatter = classname => classname) => props => (
  recurFormatClassname(props, formatter)
);

const recurFormatClassname = (props, formatter) => {
  if (typeof props !== 'object') {
    return props;
  }
  const updatedProps = {};
  Object.keys(props).forEach((key) => {
    if (key === 'className') {
      updatedProps[key] = formatter(props.className);
    } else {
      updatedProps[key] = recurFormatClassname(props[key], formatter);
    }
  });

  return updatedProps;
};

const defaultFormatter = (classes) => {
  const classesArr = classes.split(' ').filter(classname => classname !== 'undefined');
  return classnames(...classesArr);
};

export { formatPropsClassnameBase };
export default formatPropsClassnameBase(defaultFormatter);
