import React from 'react';
import { deepmerge } from '../../helpers';
import Component from './Component';
import defaultPropImport from './defaultProp';

const combinedProps = props => deepmerge.all([
  { backgroundImageProp: defaultProp },
  props,
]);

export const ComponentBase = Component;
export const defaultProp = defaultPropImport;
export default props => <Component {...combinedProps(props)} />;
