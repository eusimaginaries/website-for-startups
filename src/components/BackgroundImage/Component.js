import PropType from 'prop-types';
import { deepmerge } from '../../helpers';

const Component = ({ backgroundImageProp, component, ...others }) => {
  const { imgSrc, ...bgOthers } = backgroundImageProp;
  const style = { backgroundImage: `url(${imgSrc})` };
  const allProps = deepmerge.all([bgOthers, others, { style }]);
  return component({ ...allProps });
};

Component.propTypes = {
  backgroundImageProp: PropType.shape({
    imgSrc: PropType.string.isRequired,
  }).isRequired,
  component: PropType.func.isRequired,
};

export default Component;
