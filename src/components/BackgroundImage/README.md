# BackgroundImage #
## Description ##
Inject `style={{backgroundImage:'url(${imgSrc})'}}` into the base component.
## Usage ##
```js
<BackgroundImage {...props} component={itemProps => <SampleComp {...itemProps}/>}/>
```
## Props ##
- backgroundImageProp: **Object**(Required) Mainly to store the `imgSrc` the rest of the props will be merged with the other props for the target component.
- backgroundImageProp.imgSrc: **String**(Required) Specifies the image url that will be used for the background
- component: **Function**(Required): Callback function that returns the Component that have been injected with the background style. `(props) => <div {...props} />`.
- Sample Props:
```js
props: { 
  backgroundImageProp: {
    imgSrc: String // (Required): Specifies the image url that will be used for the background.
    // ...
  },
  sampleItemProp: {
    sampleProp: Any // Actual component props
    // ... 
  }
}
```