import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import Component, { ComponentBase, defaultProp } from './index';

const testImgUrl = 'https://via.placeholder.com/150/0000FF/808080';

describe('Crash tests', () => {
  it('renders without crashing w/ required props', () => {
    const div = document.createElement('div');
    const props = {
      headlineProp: {
        backgroundImageProp: {
          imgSrc: testImgUrl,
        },
      },
    };
    const component = compProp => <p {...compProp} />;
    ReactDOM.render(<Component {...props.headlineProp} component={component} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
});

describe('Functionality tests', () => {
  it('Returns component with background style w/o default props', () => {
    const testProp = {
      backgroundImageProp: {
        imgSrc: testImgUrl,
      },
    };
    const testComp = (
      <ComponentBase
        {...testProp}
        component={prop => <p {...prop} />}
      />
    );
    const expectedOutput = (
      <p style={{ backgroundImage: `url(${testImgUrl})` }} />
    );

    expect(renderer.create(testComp).toJSON())
      .toEqual(renderer.create(expectedOutput).toJSON());
  });

  it('Returns component with background style w/ default props', () => {
    const testProp = {
      backgroundImageProp: {
        imgSrc: testImgUrl,
      },
    };
    const testComp = (
      <Component
        {...testProp}
        component={prop => <p {...prop} />}
      />
    );
    const expectedOutput = (
      <p
        style={{
          ...defaultProp.style,
          backgroundImage: `url(${testImgUrl})`,
        }}
      />
    );

    expect(renderer.create(testComp).toJSON())
      .toEqual(renderer.create(expectedOutput).toJSON());
  });

  it('Returns component with background style w/ default props w/ additional props', () => {
    const testProp = {
      backgroundImageProp: {
        imgSrc: testImgUrl,
      },
      className: 'p-component',
    };
    const testComp = (
      <Component
        {...testProp}
        component={prop => <p {...prop} />}
      />
    );
    const expectedOutput = (
      <p
        className="p-component"
        style={{
          ...defaultProp.style,
          backgroundImage: `url(${testImgUrl})`,
        }}
      />
    );

    expect(renderer.create(testComp).toJSON())
      .toEqual(renderer.create(expectedOutput).toJSON());
  });
});
