import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import { Grid } from '@material-ui/core';

import Component from './index';

describe('Crash tests', () => {
  it('renders without crashing (vanilla)', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Component />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders without crashing (w/ props)', () => {
    const props = {
      className: 'mainclass',
      itemsProp: {
        'item-0': {
          className: 'item0',
        },
        'item-1': {
          className: 'item1',
        },
      },
    };

    const div = document.createElement('div');
    ReactDOM.render(<Component {...props} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders without crashing (w/ item, w/o props)', () => {
    const items = [
      {
        key: 'item-0',
        component: props => <div {...props} />,
      }, {
        key: 'item-1',
        component: props => <p {...props} />,
      },
    ];

    const div = document.createElement('div');
    ReactDOM.render(<Component items={items} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders without crashing (w/ item, w/ props)', () => {
    const items = [
      {
        key: 'item-0',
        component: props => <div {...props} />,
      }, {
        key: 'item-1',
        component: props => <p {...props} />,
      },
    ];
    const props = {
      className: 'mainclass',
      itemsProp: {
        'item-0': {
          className: 'item0',
        },
        'item-1': {
          className: 'item1',
        },
      },
    };

    const div = document.createElement('div');
    ReactDOM.render(<Component items={items} {...props} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
});

describe('Functionality tests', () => {
  it('Returns without Component when there are no items', () => {
    const testComp = (
      <div>
        <Component />
      </div>
    );
    const expectedOutput = (
      <div />
    );

    expect(renderer.create(testComp).toJSON())
      .toEqual(renderer.create(expectedOutput).toJSON());
  });

  it('Renders items correctly when there are items', () => {
    const items = [
      {
        key: 'item-0',
        component: props => <div {...props} />,
      }, {
        key: 'item-1',
        component: props => <p {...props} />,
      },
    ];
    const testComp = (
      <Component items={items} />
    );
    const expectedOutput = (
      <Grid container spacing={24}>
        <Grid item>
          <div />
        </Grid>
        <Grid item>
          <p />
        </Grid>
      </Grid>
    );

    expect(renderer.create(testComp).toJSON())
      .toEqual(renderer.create(expectedOutput).toJSON());
  });

  it('Renders items w/ props correctly when there are items and props', () => {
    const items = [
      {
        key: 'item-0',
        component: props => <div {...props} />,
      }, {
        key: 'item-1',
        component: props => <p {...props} />,
      },
    ];
    const props = {
      className: 'main-div',
      itemsProp: {
        'item-0': {
          className: 'griditem-0',
          gridItemProp: {
            className: 'gi-0-comp',
          },
        },
        'item-1': {
          gridItemProp: {
            className: 'gi-1-comp',
          },
        },
      },
    };
    const testComp = (
      <Component items={items} {...props} />
    );
    const expectedOutput = (
      <Grid container spacing={24} className="main-div">
        <Grid item className="griditem-0">
          <div className="gi-0-comp" />
        </Grid>
        <Grid item>
          <p className="gi-1-comp" />
        </Grid>
      </Grid>
    );

    expect(renderer.create(testComp).toJSON())
      .toEqual(renderer.create(expectedOutput).toJSON());
  });

  it('Renders items w/ props correctly when there are items and 1 prop missing', () => {
    const items = [
      {
        key: 'item-0',
        component: props => <div {...props} />,
      }, {
        key: 'item-1',
        component: props => <p {...props} />,
      },
    ];
    const props = {
      className: 'main-div',
      itemsProp: {
        'item-1': {
          gridItemProp: {
            className: 'gi-1-comp',
          },
        },
      },
    };
    const testComp = (
      <Component items={items} {...props} />
    );
    const expectedOutput = (
      <Grid container spacing={24} className="main-div">
        <Grid item>
          <div />
        </Grid>
        <Grid item>
          <p className="gi-1-comp" />
        </Grid>
      </Grid>
    );

    expect(renderer.create(testComp).toJSON())
      .toEqual(renderer.create(expectedOutput).toJSON());
  });
});
