import React from 'react';
import PropType from 'prop-types';
import { Grid } from '@material-ui/core';

const renderItem = (gridItem, props = {}) => {
  const { key, component } = gridItem;
  // eslint-disable-next-line react/destructuring-assignment
  const itemProp = props[key] || {}; // Dynamic variable name
  const { gridItemProp = {}, ...others } = itemProp;

  return (
    <Grid item key={key} {...others}>
      {component(gridItemProp)}
    </Grid>
  );
};

const Component = ({ items, itemsProp, ...others }) => {
  if (!items.length) {
    return null;
  }

  return (
    <Grid container spacing={24} {...others}>
      {items.map(item => renderItem(item, itemsProp))}
    </Grid>
  );
};

Component.propTypes = {
  items: PropType.arrayOf(PropType.shape({
    key: PropType.string.isRequired,
    component: PropType.func.isRequired,
  })),
  itemsProp: PropType.shape({}),
};

Component.defaultProps = {
  items: [],
  itemsProp: {},
};

export default Component;
