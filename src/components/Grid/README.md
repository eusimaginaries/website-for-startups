# Grid #
## Description ##
Converts an array of items into a grid.
## Usage ##
```js
<Component items={items} />
```
## Props ##
- items: **Array of objects**(Required)  Represents the items that should be created.
- itemProps: **Object** Specifies the props to be passed down to the items. Level1 keys should be the ids of the items.
- itemProps.`${item.id}`.gridItemProp: **Object** Props that are for the component itself.
- Sample Props:
```js
props: {
  items: [{
    key: 'sample-item',
    component: props => <div {...props} />,
  }],
  sampleMainProps: 'sample',              // Grid container props
  itemsProp: {
    'sample-item': {                      // Grid item props
      sampleGridItemLevelProp: 'true',
      gridItemProp: {
        'sample-component': 'some prop'   // Component props
      },
    },
  },
}
```
