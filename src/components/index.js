import * as materialUi from '@material-ui/core';
import CusHeadline from './Headline';
import CusGrid from './Grid';
import CusBackgroundImage from './BackgroundImage';
import CusStyle from './Style';

const customComponents = { CusHeadline, CusGrid, CusBackgroundImage, CusStyle };
const combinedComponents = { ...materialUi, ...customComponents };

export const {
  CusGrid: Grid,
  CusHeadline: Headline,
  CusBackgroundImage: BackgroundImage,
  CusStyle: Style,
} = combinedComponents;
export default combinedComponents;
