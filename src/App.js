import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import logo from './logo.svg';
import './App.css';
import { Landing } from './pages';
import { props as propsConfig, styles as stylesConfig } from './config';
import { Style } from './components';

const SamplePage = () => (
  <div className="App">
    <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <p>
        Edit
        <code>
          src/App.js
        </code>
        and save to reload.
      </p>
      <a
        className="App-link"
        href="https://reactjs.org"
        target="_blank"
        rel="noopener noreferrer"
      >
        Learn React
      </a>
    </header>
  </div>
);

const StyledApp = () => (
  <Style styles={stylesConfig} configProps={propsConfig} component={props => <App {...props} />} />
);

const App = ({ landing }) => (
  <Router>
    <Switch>
      <Route path="/landing" component={() => <Landing {...landing} />} />
      <Route path="/" component={SamplePage} />
    </Switch>
  </Router>
);
export default StyledApp;
