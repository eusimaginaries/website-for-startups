const styles = theme => ({
  minHeight100: {
    minHeight: '100vh',
  },
  headline: {
    margin: '30vh 100px 0 100px',
  },
  stitchContainer: {
    outline: '1px dashed #98abb9',
    outlineOffset: '-5px',
    backgroundColor: '#556068',
    padding: '100px',
    '-webkit-box-shadow': '2px 2px 2px #000',
    '-moz-box-shadow': '2px 2px 2px #000',
    'box-shadow': '2px 2px 2px #000',
  },
  textWhite: {
    color: 'white',
  },
});

export default styles;
