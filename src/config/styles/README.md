# Styles #
## Description ##
This is the centralised location of speciying the different stylings.
Overwriting order will be as such: `default -> custom`.
## Usage ##
```js
{
  "id":{
    fontSize: 10,
  }
}
```
**Note:** This component uses `withStyle` whereby a classname will be returned via `classes` that will reference the particular section of the CSS.

