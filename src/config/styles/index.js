import { deepmerge } from '../../helpers';
import customStylesImport from './custom';
import defaultStylesImport from './default';

const combinedStyles = theme => deepmerge.all([
  defaultStylesImport(theme),
  customStylesImport(theme),
]);

export const customStyles = customStylesImport;
export const defaultStyles = defaultStylesImport;
export default combinedStyles;
