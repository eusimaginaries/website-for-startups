const props = ({ classes }) => ({
  landing: {
    contentProp: {
      className: `${classes.minHeight100}`,
      backgroundImageProp: {
        imgSrc: 'https://images.pexels.com/photos/531880/pexels-photo-531880.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
      },
      gridProp: {
        alignItems: 'center',
        itemsProp: {
          'grid-headline': {
            xs: 12,
            gridItemProp: {
              headlineProp: {
                className: `${classes.stitchContainer} ${classes.headline}`,
                titleProp: {
                  className: `${classes.textWhite}`,
                  variant: 'h1',
                  align: 'center',
                  text: 'Eus Imaginaries',
                },
                subtitleProp: {
                  variant: 'h4',
                  align: 'center',
                  text: 'Where imaginations are not just imaginations',
                },
              },
            },
          },
        },
      },
    },
  },
});

export default props;
