import { deepmerge } from '../../helpers';
import customPropsImport from './custom';
import defaultPropsImport from './default';

const combinedProps = (extra = {}) => (
  deepmerge.all([defaultPropsImport(extra), customPropsImport(extra)])
);

export default combinedProps;
