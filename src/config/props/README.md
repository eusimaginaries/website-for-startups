# Props #
## Description ##
This is the centralised location of speciying the different props.
Overwriting order will be as such: `default -> custom`.
## Usage ##
```js
({classes})=>({
  sampleProp: 'x', 
  className: `${classes.sampleId}`
})
```
## Props ##
- classes: **Object** returned when used with the `withStyle` component. It can be used as a literal variable.
