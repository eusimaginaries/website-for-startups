import deepmerge from './index';

describe('Functional test', () => {
  it('concats classNames', () => {
    const obj1 = { className: 'class1' };
    const obj2 = { className: 'class2' };
    const expected = { className: 'class1 class2' };

    expect(deepmerge(obj1, obj2)).toEqual(expected);
  });

  it('concats classNames all', () => {
    const obj1 = { className: 'class1' };
    const obj2 = { className: 'class2' };
    const obj3 = { className: 'class3' };
    const expected = { className: 'class1 class2 class3' };

    expect(deepmerge.all([obj1, obj2, obj3])).toEqual(expected);
  });
});
