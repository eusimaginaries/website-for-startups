// Code from: https://github.com/TehShrike/deepmerge
// Needed some additional features.
import defaultIsMergeableObject from './isMergeableObject';

const emptyTarget = val => (Array.isArray(val) ? [] : {});

const cloneUnlessOtherwiseSpecified = (value, options) => (
  (options.clone !== false && options.isMergeableObject(value))
    ? deepmerge(emptyTarget(value), value, options)
    : value
);

const defaultArrayMerge = (target, source, options) => (
  target.concat(source).map(element => (
    cloneUnlessOtherwiseSpecified(element, options)
  ))
);

const mergeObject = (target, source, options) => {
  const destination = {};
  if (options.isMergeableObject(target)) {
    Object.keys(target).forEach((key) => {
      destination[key] = cloneUnlessOtherwiseSpecified(target[key], options);
    });
  }
  Object.keys(source).forEach((key) => {
    if (key === 'className' && typeof source[key] === 'string' && typeof target[key] === 'string') {
      // VerySpecific code
      destination[key] = `${target[key]} ${source[key]}`;
    } else if (!options.isMergeableObject(source[key]) || !target[key]) {
      destination[key] = cloneUnlessOtherwiseSpecified(source[key], options);
    } else {
      destination[key] = deepmerge(target[key], source[key], options);
    }
  });
  return destination;
};

const deepmerge = (target, source, inputOptions = {}) => {
  const options = {
    arrayMerge: defaultArrayMerge,
    isMergeableObject: defaultIsMergeableObject,
    ...inputOptions,
  };

  const sourceIsArray = Array.isArray(source);
  const targetIsArray = Array.isArray(target);
  const sourceAndTargetTypesMatch = sourceIsArray === targetIsArray;

  if (!sourceAndTargetTypesMatch) {
    return cloneUnlessOtherwiseSpecified(source, options);
  }
  if (sourceIsArray) {
    return options.arrayMerge(target, source, options);
  }
  return mergeObject(target, source, options);
};

deepmerge.all = (array, options) => {
  if (!Array.isArray(array)) {
    throw new Error('first argument should be an array');
  }

  return array.reduce((prev, next) => deepmerge(prev, next, options), {});
};

export default deepmerge;
