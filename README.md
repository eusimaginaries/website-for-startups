# Website for Startups
## Project Objective
This project is used to allow quick creation of websites to be used by startups.
For more detailed information please view in [Solution Document](https://docs.google.com/document/d/1o5XxslAlNdvgvq4x7-YPSVYoaCAc0rdTRLgPrQ8xVyI/edit?usp=sharing).

## First Step
This project uses `yarn` as the package manager. However, `npm` can be used without issue too.
```bash
# installing the dependencies (via yarn)
> yarn
# Available scripts
## 1. lint - To perform linting on the project. Lint config is stored in .eslintrc.json
## 2. test - Test the files using ReactScript test functionality.
## 3. start - Start the development webserver on localhost:3000
> yarn #<<insert script name >>#
```

## Components Documentation
### Configurations
This project allow fast modifications via the use of configurations. This is stored in `.../src/config`.
Configurations comes as a pair; default and custom. Custom configurations will override the default configurations via deep merge. This therefore allows more flexibility of only overriding where necessary.
Note: Configurations **SHOULD** only affect the props and stylings. Components itself should be created in the Page so as to reduce unncessary complexity.
### Building blocks
Stored in `.../src/components`, these blocks are used to create the various pages and can be wired up to the configurations.
**Material-UI** components are also exposed in this project and can be used when needed.
### Components
- [BackgroundImage](src/components/BackgroundImage/README.md)
- [Grid](src/components/Grid/README.md)
- [Headline](src/components/Headline/README.md)
- [Style](src/components/Style/README.md)
### Sample Pages
- [Landing Page](src/pages/Landing/README.md)
### Configurations
- [Props](src/config/props/README.md)